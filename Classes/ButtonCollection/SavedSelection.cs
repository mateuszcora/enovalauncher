﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

namespace EnovaRunner
{
    [XmlRoot("Config")]
    public class SavedConfig
    {
        [XmlElement("Extension")]
        public string LastExtLoaded { get; set; }
        [XmlElement("Version")]
        public string LastVersionLoaded { get; set; }
        [XmlElement("ExePath")]
        public string ExePath { get; set; }
        [XmlElement("ExtPath")]
        public string ExtPath { get; set; }

        protected static readonly string DefaultExePath = "C:\\Program Files (x86)\\Soneta\\";
        protected static readonly string DefaultExtPath = "C:\\Ext\\";
        protected static readonly string settingsFolder = "EnovaLauncher";
        protected static readonly string savedFilename = "config.xml";
        protected static readonly string SettingsPathSystemVariable = "APPDATA";

        public void Save()
        {
            string path = GetSettingsPathCreateIfNotExists();
            using (TextWriter writer = new StreamWriter(path))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(SavedConfig));
                serializer.Serialize(writer, this);
            }
        }

        
        public static SavedConfig Load()
        {
            SavedConfig config = null;
            try
            {
                config = LoadFromFile();
            }
            catch(Exception ex)
            {
                config = GenerateEmpty();
            }

            config = SetDefaultsIfEmpty(config);

            return config;
        }
        protected static SavedConfig LoadFromFile()
        {
            SavedConfig config = null;
            using (TextReader reader = GetReader())
            {
                XmlSerializer serializer = new XmlSerializer(typeof(SavedConfig));
                config = serializer.Deserialize(reader) as SavedConfig;
            }
            return config;
        }
        protected static SavedConfig GenerateEmpty()
        {
            SavedConfig config = new SavedConfig();
            config.Save();
            return config;
        }
        protected static SavedConfig SetDefaultsIfEmpty(SavedConfig config)
        {
            /* 
             * default version and extensions don't matter - it's not my job to handle this
             * if defaults are empty, no version and extensions are selected
             * */
            if (config.ExePath == null || config.ExePath == string.Empty)
                config.ExePath = DefaultExePath;
            if (config.ExtPath == null || config.ExtPath == string.Empty)
                config.ExtPath = DefaultExtPath;
            return config;
        }

        protected static TextReader GetReader()
        {
            string path = GetSettingsPathCreateIfNotExists();
            TextReader reader = new StreamReader(path);
            return reader;
        }

        protected static string GetSettingsPathCreateIfNotExists()
        {

            string path = "";

            path = Environment.GetEnvironmentVariable(SettingsPathSystemVariable);
            path += "\\" + settingsFolder;
            if (!Directory.Exists(path))
            {

                Directory.CreateDirectory(path);
            }
            path += "\\" + savedFilename;
            return path;
        }
        
    }
}
