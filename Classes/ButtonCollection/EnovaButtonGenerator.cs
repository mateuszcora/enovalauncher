﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace EnovaRunner
{
    class EnovaButtonGenerator
    {
        public FlowLayoutPanel ExtensionPanel {get; set;}
        public FlowLayoutPanel VersionPanel { get; set; }
        public EnovaLauncher Launcher { get; set; }
        protected readonly string pathIdentifier = "enova365 ";


        public void  GenerateButtons()
        {
            GenerateExtensions();
            GenerateVersions();
        }

        public void SelectLastButtons(SavedConfig Saved)
        {
            foreach(Control button in ExtensionPanel.Controls)
            {
                if(button is ExtensionsButton &&
                    button.Text == Saved.LastExtLoaded)
                {
                    (button as EnovaButton).ForceChange();
                    button.Focus();
                }
            }
            foreach(Control button in VersionPanel.Controls)
            {
                if(button is VersionButton && 
                    button.Text == Saved.LastVersionLoaded)
                {
                    (button as EnovaButton).ForceChange();
                    button.Focus();
                }
            }
        }
        protected void GenerateVersions()
        {
            SavedConfig config = SavedConfig.Load();
            List<string> Directories = GetFolders(config.ExePath);
            Directories.Reverse();
            foreach(string directory in Directories)
            {
                if (directory.Contains(pathIdentifier))
                {
                    VersionButton button = new VersionButton();
                    button.ExePath = directory + "SonetaExplorer.exe";
                    button.Text = GetVersionNumber(directory);
                    button.ParamsChange += new EventHandler<ParamsArgs>(Launcher.OnVersionParamsChange);
                    VersionPanel.Controls.Add(button);

                    VersionButton button32 = new VersionButton();
                    button32.ExePath = directory + "SonetaExplorer32.exe";
                    button32.Text = GetVersionNumber(directory) + " (x86)";
                    button32.ParamsChange += new EventHandler<ParamsArgs>(Launcher.OnVersionParamsChange);
                    VersionPanel.Controls.Add(button32);
                }
            }
            try
            {
                VersionButton first = VersionPanel.Controls.OfType<VersionButton>().First();
                first.Checked = true;
                first.ForceChange();
                first.Focus();
            }
            catch (Exception ex) { }

        }
        string GetVersionNumber(string path)
        {
            string number = "";
            int numberStartIndex = path.IndexOf(pathIdentifier) + pathIdentifier.Length;
            number = path.Substring(numberStartIndex, 9);
            return number;
        }
        protected void GenerateExtensions()
        {
            SavedConfig config = SavedConfig.Load();
            foreach(string directory in GetFolders(config.ExtPath))
            {
                ExtensionsButton button = new ExtensionsButton();
                button.ExtPath = directory;
                button.Text = directory.Substring(config.ExtPath.Length, directory.Length - config.ExtPath.Length - 1);
                button.ParamsChange += new EventHandler<ParamsArgs>(Launcher.OnExtParamsChange);
                ExtensionPanel.Controls.Add(button);
            }
            try
            {
                ExtensionsButton first = ExtensionPanel.Controls.OfType<ExtensionsButton>().First();
                first.Checked = true;
                first.ForceChange();
            }
            catch (Exception ex) { }

        }
        List<string> GetFolders(string Path)
        {
            List<string> Folders = new List<string>();
            string[] directories = Directory.GetDirectories(Path);
            foreach (string dir in directories)
            {
                Folders.Add(dir + "\\");
            }
            return Folders;
        }

    }
}
