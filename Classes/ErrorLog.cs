﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnovaRunner
{
    public class ErrorLog
    {
        public static ErrorLabel Label { set => label = value; }
        protected static ErrorLabel label;
        protected static ErrorLog log = null;
        ErrorLog()
        {

        }
        public static void Log(string error)
        {
            Log(error, null);
        }
        public static void Log(string error, Exception ex)
        {
            if(label != null)
            {
                label.LogError(error, ex);
            }
            else
            {
                throw new Exception("Log label not found");
            }
            
        }
    }
}
