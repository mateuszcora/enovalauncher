﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace EnovaRunner
{
    public class ErrorLabel : Label
    {
        protected readonly int ErrorDisplayTimeInMiliseconds = 20000;
        protected Color SafeColor;
        protected Color ErrorColor = Color.Tomato;
        protected string ErrorText;
        public ErrorLabel() : base()
        {
            SafeColor = BackColor;
            DoubleClick += OnDoubleClick;
        }
        public void LogError(string error, Exception ex)
        {
            Text = error;
            if (ex != null)
            {
                ErrorText += Environment.NewLine + Environment.NewLine + ex.Message;
            }
            BackColor = ErrorColor;

            Timer timer = new Timer();
            timer.Interval = ErrorDisplayTimeInMiliseconds;
            timer.Tick += OnDisplayTimePassed;
        }

        public void OnDisplayTimePassed(object sender, EventArgs args)
        {
            if(sender is Timer timer)
            {
                Reset();
                timer.Dispose();
            }
        }

        protected void Reset()
        {
            BackColor = SafeColor;
            Text = string.Empty;
            ErrorText = string.Empty;
        }
        public void OnDoubleClick(object sender, EventArgs args)
        {
            MessageBox.Show(Text + ErrorText, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

    }
}
