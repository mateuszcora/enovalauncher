﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace EnovaRunner
{
    
    public delegate void onSuccessfulRun();
    class EnovaLauncher
    {
        protected SavedConfig selection;
        protected string VersionPath;
        protected string ExtensionsPath;
        protected readonly string ArgsString = " /nodbextensions /extpath=";
        onSuccessfulRun ClosingFunction;

        public EnovaLauncher(onSuccessfulRun function)
        {
            VersionPath = null;
            ExtensionsPath = null;
            ClosingFunction = function;
            selection = new SavedConfig();
        }
        public virtual void OnVersionParamsChange(object sender, ParamsArgs args)
        {
            VersionPath = args.Value;
            if(sender is EnovaButton)
            {
                EnovaButton button = sender as EnovaButton;
                selection.LastVersionLoaded = button.Text;
                ListItemController.AktualizujListe(ExtensionsPath);
            }
        }
        public virtual void OnExtParamsChange(object sender, ParamsArgs args)
        {
            ExtensionsPath = args.Value;
            if(sender is EnovaButton)
            {
                EnovaButton button = sender as EnovaButton;
                selection.LastExtLoaded = button.Text;
                ListItemController.AktualizujListe(ExtensionsPath);
            }
        }
        public void Launch(object sender, EventArgs args)
        {
            try
            {
                DoLaunch();
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }
        protected void DoLaunch()
        {
            selection.Save();
            if (VersionPath == null)
            {
                System.Windows.Forms.MessageBox.Show("Please select a version");
                return;
            }

            ProcessStartInfo startInfo = new ProcessStartInfo(VersionPath, ArgsString  + ExtensionsPath );
            if (ExtensionsPath == null)
            {
                startInfo.Arguments = "";
            }
            //System.Windows.Forms.MessageBox.Show(startInfo.FileName + " " + startInfo.Arguments);
            Process.Start(startInfo);
            ClosingFunction();
        }
    }
}
