﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace EnovaRunner
{
    public class ListItemController
    {
        public static ListView Lista { get; set; }
        public static void AktualizujListe(string Scieżka)
        {
            if(!Directory.Exists(Scieżka))
            {
                throw new Exception("Nie znaleziono folderu " + Scieżka);
            }
            IEnumerable<string> rozszerzenia = Directory.EnumerateFiles(Scieżka, "*.dll");
            Lista.Items.Clear();
            foreach(string scieżkaPliku in rozszerzenia)
            {
                string nazwaPliku = Path.GetFileName(scieżkaPliku);
                Lista.Items.Add(new ListViewItem(nazwaPliku));
            }
        }
    }
}
