﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EnovaRunner
{
    class EnovaButton : CheckBox
    {

        public event EventHandler<ParamsArgs> ParamsChange;
        public EnovaButton() : base()
        {
            this.Appearance = Appearance.Button;
            this.Width = 110;
            this.Click += OnClick;
        }

        public void OnClick(object sender, EventArgs args)
        {
            UpdateSiblings();
            NotifyAboutChange();
        }
        public void ForceChange()
        {
            this.Checked = true;
            OnClick(null, EventArgs.Empty); 
        }
        protected void UpdateSiblings()
        {
            foreach (Control control in Parent.Controls)
            {
                if (control is EnovaButton && control != this)
                {
                    CheckBox otherControl = control as CheckBox;
                    otherControl.Checked = false;
                }
            }
        }

        protected void NotifyAboutChange()
        {
            ParamsChange?.Invoke(this, GenerateParams());
        }
        protected virtual ParamsArgs GenerateParams()
        {
            throw new Exception("Should not have ran this");
        }
    }
}
