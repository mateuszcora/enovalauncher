﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnovaRunner
{
    class ExtensionsButton : EnovaButton
    {
        public string ExtPath { get; set; }
        
        protected override ParamsArgs GenerateParams ()
        {
            ParamsArgs args = new ParamsArgs();
            args.Value = ExtPath;
            return args;
        }

    }
}
