﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnovaRunner
{
    class VersionButton : EnovaButton
    {
        public string ExePath { get; set; }

        protected override ParamsArgs GenerateParams()
        {
            ParamsArgs args = new ParamsArgs();
            args.Value = ExePath;
            return args;
        }
    }
}
