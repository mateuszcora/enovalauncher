﻿using System;

namespace EnovaRunner
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.Extensions = new System.Windows.Forms.FlowLayoutPanel();
            this.Versions = new System.Windows.Forms.FlowLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.LaunchButton = new System.Windows.Forms.Button();
            this.listView1 = new System.Windows.Forms.ListView();
            this.Dodatek = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.errorLabel2 = new EnovaRunner.ErrorLabel();
            this.TableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // TableLayoutPanel1
            // 
            this.TableLayoutPanel1.ColumnCount = 3;
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.61095F));
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 41.34897F));
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.04008F));
            this.TableLayoutPanel1.Controls.Add(this.Extensions, 1, 0);
            this.TableLayoutPanel1.Controls.Add(this.Versions, 0, 0);
            this.TableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 2, 0);
            this.TableLayoutPanel1.Controls.Add(this.errorLabel2, 0, 2);
            this.TableLayoutPanel1.Location = new System.Drawing.Point(3, 24);
            this.TableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.TableLayoutPanel1.Name = "TableLayoutPanel1";
            this.TableLayoutPanel1.RowCount = 3;
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.TableLayoutPanel1.Size = new System.Drawing.Size(1363, 484);
            this.TableLayoutPanel1.TabIndex = 0;
            this.TableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.TableLayoutPanel1_Paint);
            // 
            // Extensions
            // 
            this.Extensions.Location = new System.Drawing.Point(353, 4);
            this.Extensions.Margin = new System.Windows.Forms.Padding(4);
            this.Extensions.Name = "Extensions";
            this.Extensions.Size = new System.Drawing.Size(552, 439);
            this.Extensions.TabIndex = 1;
            // 
            // Versions
            // 
            this.Versions.Location = new System.Drawing.Point(4, 4);
            this.Versions.Margin = new System.Windows.Forms.Padding(4);
            this.Versions.Name = "Versions";
            this.Versions.Size = new System.Drawing.Size(340, 439);
            this.Versions.TabIndex = 2;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.42042F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 79.57958F));
            this.tableLayoutPanel2.Controls.Add(this.LaunchButton, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.listView1, 1, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(916, 4);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(443, 439);
            this.tableLayoutPanel2.TabIndex = 3;
            // 
            // LaunchButton
            // 
            this.LaunchButton.Location = new System.Drawing.Point(4, 4);
            this.LaunchButton.Margin = new System.Windows.Forms.Padding(4);
            this.LaunchButton.Name = "LaunchButton";
            this.LaunchButton.Size = new System.Drawing.Size(80, 431);
            this.LaunchButton.TabIndex = 1;
            this.LaunchButton.Text = "Uruchom";
            this.LaunchButton.UseVisualStyleBackColor = true;
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Dodatek});
            this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView1.Location = new System.Drawing.Point(94, 4);
            this.listView1.Margin = new System.Windows.Forms.Padding(4);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(345, 431);
            this.listView1.TabIndex = 2;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.List;
            // 
            // Dodatek
            // 
            this.Dodatek.Width = 260;
            // 
            // errorLabel2
            // 
            this.TableLayoutPanel1.SetColumnSpan(this.errorLabel2, 3);
            this.errorLabel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.errorLabel2.Location = new System.Drawing.Point(3, 447);
            this.errorLabel2.Name = "errorLabel2";
            this.errorLabel2.Size = new System.Drawing.Size(1357, 37);
            this.errorLabel2.TabIndex = 4;
            this.errorLabel2.Text = "errorLabel2";
            this.errorLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1369, 511);
            this.Controls.Add(this.TableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainWindow";
            this.Text = "Enova";
            this.TableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel TableLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel Extensions;
        private System.Windows.Forms.FlowLayoutPanel Versions;

        private EnovaLauncher Launcher;
        private EnovaButtonGenerator ButtonGenerator;

        protected void InitializeProgram()
        {
            ErrorLog.Label = errorLabel2;
            
            Launcher = new EnovaLauncher(new onSuccessfulRun(this.Close));
            ListItemController.Lista = listView1;

            ButtonGenerator = new EnovaButtonGenerator();
            ButtonGenerator.ExtensionPanel = Extensions;
            ButtonGenerator.VersionPanel = Versions;
            ButtonGenerator.Launcher = Launcher;
            try
            {
                ButtonGenerator.GenerateButtons();
            }
            catch(Exception ex)
            {
                ErrorLog.Log("Problem with creating buttons", ex);
            }
            LaunchButton.Click += new System.EventHandler(Launcher.Launch);

            try
            {
                ButtonGenerator.SelectLastButtons(SavedConfig.Load());
            }
            catch(Exception ex)
            {
                ErrorLog.Log("Problem with loading last selection", ex);
            }
            

        }

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button LaunchButton;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader Dodatek;
        private ErrorLabel errorLabel1;
        private ErrorLabel errorLabel2;
    }
}

